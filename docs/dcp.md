# Digital Collaboration Platform

Our [Digital Collaboration Platform](https://community.gaia-x.eu/), or DCP, is a [NextCloud](https://nextcloud.com/) instance that all Working Group (WG) members have access to.

The platform enables to:

- share and access documents from all the Committee and WG
- edit document collaboratively, using [Collabora](https://www.collaboraoffice.com/)
- chat with other DCP users
