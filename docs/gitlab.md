# Gitlab

Our Gitlab [Gaia-X project](https://gitlab.com/gaia-x/) is the main platform to work on

- document such as:
    - the [Architecture document](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture) which is mainained by the [Architecture WG](https://gaia-x.gitlab.io/gaia-x-community/operational-handbook/wg_architecture/) through the [ADR process](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/wikis/home) 
        
    - the [Technical Handbook](https://gitlab.com/gaia-x/gaia-x-community/technical-handbook)

- [community projects](https://gitlab.com/gaia-x/gaia-x-community)
- prototypes:
    - [catalogue search engine prototype](https://gitlab.com/gaia-x/gaia-x-community/catalogue-search-engine-prototype)
    - ...
- Tips & Tricks
    - [Personal Todo list](https://gitlab.com/dashboard/todos) (also available checkmark link in Gitlab menu)
