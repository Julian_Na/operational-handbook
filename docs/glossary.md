# Glossary

## WG

see [Working Group](#Working Group)

## WP

see [Work Package](#Work Package)

## Working Group

Group open to Gaia-X AISBL Members only.

## Work Package

Group open to everybody.
