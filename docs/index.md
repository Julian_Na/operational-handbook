# Welcome to the Gaia-X Operational Handbook

![logo](images/gaiax logo.svg)

## Gaia-X organisation

![logo](images/cto orga.png)

## CTO organisation
<!-- <style>
svg[id^="mermaid-"] { min-width: 200px; max-width: 700px; }
</style> -->

```mermaid
flowchart BT;

    WorkingGroups --> tc[[Technical Committee]]

    WorkPackages --> wgn

    subgraph WorkingGroups
        wg1[Working Group 1]
        swg1[Sub-Working Group 1] --> wg1
        wg2[Working Group 2]
        wgn[Working Group N]
    end

    subgraph WorkPackages
        wp1([Work Package 1])
        wp2([Work Package 2])
        swp2([Sub-Working Group 1]) --> wp2
        wpn([Work Package N])
    end
```


## Working Groups

All Working Groups deliverables must be stored on one of the Gaia-X platforms. See the `Tools` section (DCP, Gitlab, ...)

The Working Groups report to the Technical Committee.

### Federation Services WG

The Federation Services/OSS Working Group shall consist of Members which drive forward the GAIA-X Federation Services (GXFS), maintaining the GXFS and coordinating the corresponding open-source repositories.

```mermaid
flowchart BT

    subgraph WorkingGroup
        wgfed[Federation Services]
    end

    subgraph WorkPackages
        wpcatalog([Catalogue])
        wpsd([Self-Description])
        wpportal([Portal & User Interface])
        wpmvg([Minimum Viable Gaia-X])
        wpcompliance([Compliance])
    end

    WorkPackages --> wgfed

    prc[[Policy Rules Committee]] -.- wpcompliance
```

### Architecture WG

The Architecture TC Working Group shall consist of Members to elevate, create and provide oversight to the GAIA-X Architecture of Standards, including, but not limited to the specification of the related Application Framework for interaction with the GAIA-X Federation Services.

```mermaid
flowchart BT

    subgraph WorkingGroup
        wgarch[Architecture]
    end

    subgraph WorkPackages
        wpaos([Architecture of Standards])
        wpiam([Identity & Trust])
        wpdata([Data Sovereignty])
        wpinterconnec([Interconnection])
        wpinfra([Infrastructure])

        subgraph Infrastructure
            wpstorage([Storage]) --> wpinfra
            wpnetwork([Network]) --> wpinfra
            wpcompute([Compute]) --> wpinfra
        end

        wpinterconnec -.- Infrastructure

    end

    WorkPackages --> wgarch
```

### X-Association WG

The X-Association Working Group shall consist of Members, as well as of representatives of other formally liaised associations (who are not a Member of the Association) to elevate and represent their associations' requirements to the standard. This group is the contact point for organizations interested in cooperation with the Association.

```mermaid
flowchart BT

    subgraph WorkingGroup
        wgxass[X-Association]
    end

    subgraph Associations
        fiware[/Fiware/]
        idsa[/IDSA/]
        bdva[/BDVA/]
        more[/.../]
    end

    Associations -.- wgxass


```

### Portfolio WG

The Portfolio Working Group shall consist of Members to develop and govern GAIA-X portfolio management process, develop and maintain the GAIA-X product roadmap based on user, provider and architecture requirements and the GAIA-X value proposition. The Portfolio Working Group shall develop the GAIA-X value propositions for GAIA-X service providers and customers of GAIA-X Service Providers and communicate the value proposition statement inside and outside of GAIA-X.

```mermaid
flowchart BT

    subgraph WorkingGroup
        wgportf[Portfolio]
    end

    subgraph WorkPackages
        wppsb[Product & Service board]
    end

    WorkPackages --> wgportf
```

### User WG

The User Working Group shall consist of Members, which shall carry the scope of user requirements for the Association and interact with the various user domains of the GAIA-X hubs to elevate and represent their user requirements to the standard.

```mermaid
flowchart BT

    subgraph WorkingGroup
        wguser[User]
    end

    dsbc[[Dataspace & Business Committee]] -.- wguser

    hub1[[Hub FR]] -.- dsbc
    hub2[[Hub DE]] -.- dsbc
    hub3[[Hub IT]] -.- dsbc
    hub4[[Hub ...]] -.- dsbc
```

### Provider WG

The Provider Working Group shall consist of Members, which shall carry the scope of provider requirements for the Association and elevate and represent their provider requirements to the standard.

```mermaid
flowchart BT

    subgraph WorkingGroup
        wgprov[Provider]
    end

    subgraph WorkPackages
        wpscs[Sovereign Cloud Stack]
    end

    WorkPackages --> wgprov
```

## How to join a Working Group ?

Get in touch with the staff facilitating the working groups:
- through the Talk feature on the [DCP](dcp.md) (write to WG-Support)
- through an e-mail to the [mailing-list](mailing-list.md) <wg-support>

Criteria to become a participant in a TC Working Group:
1. AISBL Membership of the organization the applicant is working for
2. No further participant in the specific WG out of organization the applicant is working for

Additional, specific criteria apply for the specific working groups (tba). 

## Work Packages

One Work Package must report to one identified Working Group.
To join a Work Package visit [https://list.gaia-x.eu](https://list.gaia-x.eu) and subscribe to the respective mailing-list.
Be aware that not following invitations to WP meetings and not contributing to the progress of the WP can result in canceling your subscription, i. e. your participation in the WP. 

### How to create a new Work Package

A Work Package must fullfill some key requirements:

- Must have a lead. Optionally, could have a co-lead.
- Must have a clear and public charter description
    - Scope description
    - Expected deliverable (see `Deliverables` section below)
    - Out of scope description
    - A contact point (See [Mailing-list](mailing_list.md) in the `Tools` section)
- Must have a clear and identified `Working Group` parent.
- Must have a written (email) approval of above items by the parent `Working Group` lead.


## Deliverables

### Common expectation for all Groups

The commonn expectation are:

- to output documents, position paper, propositions to be reviewed by the Technical Committee.
- to give inputs for the roadmap prioritization
- to raise users - Producer and Consumer - requirements toward the Gaia-X functional and operational models to handle
    - interoperability
    - security
    - scaling

### Delivery process

```mermaid
journey
    title Contribution process
    section Contribution
      Open a Merge-Request: 5: member, non-member
      Sign the CLA: 5: member
      Sign the IPR: 5: non-member
    section Review
      Approve/Reject the Merge-Request: 5: maintainers
```

### Documents
TbD

### Source code

An important part of the work is to validate the hypothesis that might have been made during the specifications process, hence the need to prototype and to write demonstrators.

#### What is a Gaia-X prototype ?

A Gaia-X prototype is a piece of code that answers a specific question.

It can use open-source or closed source software.

The results and conclusions of the prototype must be stored and shareble with the Gaia-X Members on one of the Gaia-X platforms (See the `Tools` section).

#### What is a Gaia-X demonstrator ?

A Gaia-X demonstrator is a software implementation that answers a specific user scenario.

It must use open-source or free software.

It must be reproducacble by any Gaia-X Member.

The code and documentation must be stored on the Gaia-X Gitlab instance. (See the `Tools` section)

A running instance of the demo could be hosted under https://<demo\>.gaia-x.community
