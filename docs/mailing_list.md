# Mailing list

Gaia-X provides free access to a GNU Mailman3 server.

The service is operated by [Mailman3](https://mailman3.com/) and the server is provided by [Hetzner](https://www.hetzner.com/).

Earch WG, WP and their respective sub groups are entitled to have a dedicated mailing-list.

The mailing list naming convention is as follow:

- `<wg>-<name>@list.gaia-x.eu`
- `<wp>-<name>@list.gaia-x.eu`
- `<wg>-<name>-<subname>@list.gaia-x.eu`
- `<wp>-<name>-<subname>@list.gaia-x.eu`

To check and join the visible mailing-lists visit <https://list.gaia-x.eu>.
