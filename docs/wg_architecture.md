# Architecture Working group

The Architecture working group is responsible for:

- Definiton, development and governance of Technical Architecture and “Architecture of Standards” (AoS) of Gaia-X
- Architectural Integrity and Consistency
- Quality Control and Assessment of Technical Proposals and Interconnection across all requirements, working packages and community projects to ensure architecture compliance
- Maintaintenance of the scope of the Gaia-X Architecture

## Deliverables

- Technical Architecture Document (incl. Metamodel and I/F description), 
- Architecture of Standards, Description of the Architecture governance and compliance processes
- Change Managment: ADR Process

## ADR process

Is documented in this [GitLab Wiki](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/wikis/home)

## Gitlab permissions

By default, the Architecture document repository is accessible only to all Gaia-X members.

At the moment, every member has to ask the permission and will be by default granted `Reporter` permissions.

Any member can submit a proposal via the creation of a Merge-Request, via a fork - similar to Github process.
Any member can comment on any existing Issue and Merge-Request

Only the Technical Committee Members have the `Developer` permissions and can edit/approve/reject merge-requests.
