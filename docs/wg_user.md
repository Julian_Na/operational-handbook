# User Working group

## Charter of User Working group:

**Purpose:**  
  
Be the User Success Advocates as representatives of Data Spaces with the Technical Committee
Build the bridge with the Technical Stream of the Data Space Business Committee
Work with Data Spaces, Data Space Business Committee and with the User Hubs organization to:
- Collect documented Data Spaces definitions and/or Domain Use Cases (with their expected business value)
- Check that Data Spaces requirements and priorities (express or implied) are consistent with what GAIA-X is organizing to deliver as a Product
- Structure/prioritize requirements from data, services and infrastructure consumers and transfer them into a prioritized list of RfCs
- Deploy guidance and foster experience sharing regarding how to implement Data Spaces with GAIA-X

**Required prerequisites (input):**
- From the Architecture Working Group, it will be expected to receive (as the GAIA-X Project matures and this becomes timely feasible):
    - For Data Space High-Level Technical Design: Deployment scenarios and reusable technical architecture patterns / templates
- From the Federation Services/OSS Working Group (same – as this becomes timely feasible):
    - For Data Space Detailed Technical Design: Federation Services Specification / API Framework 

**Deliverables (output):**
- User requirements
- Requests for Change
- New community project proposals or initiatives

**Member criteria:**
- Legitimacy: representative of a Data Space, as the owner of Use Cases and/or nominated by Data Space Leadership
- Operational delivery skills: experience in IT Architecture and/or Project Management (Business Analysis and Data Modeling are a great plus)
- Availability: each member should expect potentially between 20 and 50% of their time necessary and available for GAIA-X
- Persistence over time and continuity of presence in the Working Group

**Co-Heads:**
- Stefan Ettl
- Stephan Stryhanyn

**Associated Open Work Packages:**  
  
None identified at this point
  
**Regular meeting date and time:**  
  
Mondays from 9 to 10
  
**Place of documentation:**  
  
[https://community.gaia-x.eu/apps/files/?dir=/Technical%20Committee/WG%20User&fileid=18258](https://community.gaia-x.eu/apps/files/?dir=/Technical%20Committee/WG%20User&fileid=18258)

## Onboarding Questions:

1. Which Data Space are you a representative of (as the owner of Use Cases and/or nominated by Data Space Leadership)?
2. Why do you think your organization and this person together fit the User WG Charter, key objectives, and member requirements?
3. What are your key objectives when joining the WG User?







